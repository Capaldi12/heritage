#ifndef PORTVALIDATOR_H
#define PORTVALIDATOR_H

#include <QValidator>

class PortValidator : public QValidator
{
    Q_OBJECT
public:
    PortValidator(QObject *parent);
    ~PortValidator() override;

    QValidator::State validate(QString &string, int &pos) const override;

private:
    QRegExp wrong;
};

#endif // PORTVALIDATOR_H
