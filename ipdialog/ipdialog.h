#ifndef IPDIALOG_H
#define IPDIALOG_H

#include <QDialog>
#include "ipvalidator.h"
#include "portvalidator.h"

namespace Ui {
class IPdialog;
}

class IPdialog : public QDialog
{
    Q_OBJECT

public:
    explicit IPdialog(QWidget *parent = nullptr);
    ~IPdialog();

    QStringList getIP() const;

private:
    Ui::IPdialog *ui;
    IpValidator *ival;
    PortValidator *pval;

    QString defaultPort = QString("55555");
    QString defaultIP = QString("127.0.0.1");

private slots:
    void setDefault();
};

#endif // IPDIALOG_H
