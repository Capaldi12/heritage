#include "ipvalidator.h"

IpValidator::IpValidator(QObject *parent)
    : QValidator(parent)
{
    QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    ipRegExp = QRegExp("^" + ipRange + "\\." + ipRange + "\\." + ipRange + "\\." + ipRange + "$");
    wrongExp = QRegExp("[^0-9.]");
}

IpValidator::~IpValidator() { }

QValidator::State IpValidator::validate(QString & string, int & pos) const
{
    if(string.contains(wrongExp))
    {
        prPos = pos;
        return QValidator::Invalid;
    }

    bool ok = 1;

    if(pos != 0)
    {
        int apos = pos - 1;
        bool zero = string[apos] == '0' && (apos == 0 || !string[apos - 1].isDigit());
        bool twodigits = string[apos].isDigit() && (apos > 0 && string[apos-1].isDigit()) && \
                         (QString(string[apos-1]) + QString(string[apos])).toInt() > 25;
        bool threedigit = string[apos].isDigit() && (apos > 0 && string[apos-1].isDigit()) && (apos > 1 && string[apos-2].isDigit());

        if((zero || twodigits || threedigit) && prPos < pos && string.count('.') < 3)
        {
            string.append('.');
            pos += 1;
        }

        if(string.contains("..") && string.indexOf("..") == pos - 2 && pos == string.length())
            string.replace("..", ".");

        QStringList list = string.split('.');
        int ippart;
        for(QStringList::iterator i = list.begin(); i != list.end(); i++)
        {
            if(i->length())
            {
                ippart = i->toInt();
                if(ippart > 255)
                    ok = 0;

                if(ippart == 0 && i->length() > 1)
                    ok = 0;
            }
        }
    }

    if(!ok)
    {
        prPos = pos;
        return QValidator::Invalid;
    }

    if(string.count('.') > 3)
    {
        prPos = pos;
        return QValidator::Invalid;
    }

    if(string.contains(ipRegExp))
    {
        prPos = pos;
        return QValidator::Acceptable;
    }

    prPos = pos;
    return QValidator::Intermediate;
}
