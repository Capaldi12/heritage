#ifndef IPVALIDATOR_H
#define IPVALIDATOR_H

#include <QValidator>

class IpValidator : public QValidator
{
    Q_OBJECT
public:
    IpValidator(QObject *parent);
    ~IpValidator() override;

    QValidator::State validate(QString & string, int & pos) const override;

private:
    QRegExp ipRegExp;
    QRegExp wrongExp;

    mutable int prPos;
};

#endif // IPVALIDATOR_H
