#include "portvalidator.h"

PortValidator::PortValidator(QObject *parent)
    : QValidator(parent)
{
    wrong = QRegExp("[^0-9]");
}

PortValidator::~PortValidator() { }

QValidator::State PortValidator::validate(QString & string, int & pos) const
{
    if(string.contains(wrong))
        return QValidator::Invalid;

    int repr = string.toInt();

    if(repr > 65535)
        return QValidator::Invalid;

    if(repr < 49152)
        return QValidator::Intermediate;

    return QValidator::Acceptable;
}
