#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <math.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    curID = 0;                      //no record selected
    genderChangeIgnore = false;     //don't ignore gender change
    rowChangedIgnore = false;       //don't ignore record selection

    insertHeader(1);                //adding headers to browser

    //disable buttons
    enableButtons(0);

    //connecting validator to name edit line
    QValidator *val = new NameValidator(this);
    ui->nameE->setValidator(val);

    //creating and connecting syncronization timer
    syncer = new QTimer();
    connect(syncer, SIGNAL(timeout()), this, SLOT(sync()));

    //setting ranges of dates and dates
    ui->bDateE->setMaximumDate(QDate::currentDate());
    ui->dDateE->setMaximumDate(QDate::currentDate());
    ui->bDateE->setDate(QDate::currentDate());
    ui->dDateE->setDate(QDate::currentDate());

    connect(ui->bDateE, SIGNAL(editingFinished()), this, SLOT(resetLimits())); //recheck for parents when changing birth date
    connect(ui->dDateE, SIGNAL(editingFinished()), this, SLOT(resetLimits())); //recheck for parents when changing death date

    //recalculate date ranges when something changes
    connect(ui->aliveE,  SIGNAL(toggled(bool)), this, SLOT(setDateRanges()));
    connect(ui->fatherE, SIGNAL(currentIndexChanged(int)), this, SLOT(setDateRanges()));
    connect(ui->motherE, SIGNAL(currentIndexChanged(int)), this, SLOT(setDateRanges()));
    connect(ui->genderE, SIGNAL(currentIndexChanged(int)), this, SLOT(setDateRanges()));

    connect(ui->genderE, SIGNAL(currentIndexChanged(int)), this, SLOT(genderChange())); //check if gender change is possible

    connect(ui->addB,    SIGNAL(clicked()), this, SLOT(add()));       //add record to database
    connect(ui->removeB, SIGNAL(clicked()), this, SLOT(remove()));    //remove record from database
    connect(ui->saveB,   SIGNAL(clicked()), this, SLOT(saveR()));     //save current record

    //open selected record
    connect(ui->browser, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)), this, SLOT(open(QListWidgetItem*)));

    //menu buttons
    connect(ui->clearM, SIGNAL(triggered()),this,SLOT(onClear()));
    connect(ui->fillM,  SIGNAL(triggered()),this,SLOT(onFill()));

    IPdialog dialog;    //getting ip and port
    if(dialog.exec())
    {
        QStringList ipport = dialog.getIP();
        if(ipport[0] == "0")    //0 means ok
        {
            DB = new Database(ipport[1].toLatin1().data(), ipport[2].toUShort());

            if(!checkStatus())  //check if everything ok
                QTimer::singleShot(0, this, SLOT(close()));  //exit if not
            else
            {
                ui->statusbar->showMessage("Подключение к серверу успешно", STATTIMEOUT);
                if(fillBrowser())
                    ui->statusbar->showMessage(QString("Успешно загружено ") + QString::number(DB->count()) + QString(" записей"), STATTIMEOUT);
                else
                    ui->statusbar->showMessage("База данных пуста", STATTIMEOUT);
                syncer->start(1000);    //sync every second
            }
        }
        else if (ipport[0] == "1" || ipport[0] == "3")  //1 means wrong ip, 3 means wrong both ip and port
        {
            QMessageBox::warning(this, "Ошибка!", "Некорректный ip-адрес");
            QTimer::singleShot(0, this, SLOT(close()));
        }
        else    //2 means wrong port (not in user span)
        {
            QMessageBox::warning(this, "Ошибка!", "Порт вне диапазона(49152 — 65535)");
            QTimer::singleShot(0, this, SLOT(close()));
        }
    }
    else
        QTimer::singleShot(0, this, SLOT(close())); //cancel pressed
}

MainWindow::~MainWindow()
{
    if(DB)
        delete DB;
    delete syncer;
    delete ui;
}

bool MainWindow::checkStatus() //checks status of database
{
    int status = DB->status();
    if(status)
    {
        if(status == 1)
            QMessageBox::warning(this,"Ошибка", "Ошибка на стороне сервера. База данных недоступна");
        else if(status == 2)
            QMessageBox::warning(this,"Ошибка", "Произошла неизвестная ошибка");
        else if(status == 3)
            QMessageBox::warning(this,"Ошибка", "Невозможно установить связь с сервером");
        else
            QMessageBox::warning(this,"Ошибка", "Потеряно соединение с сервером");
        return 0;
    }
    return 1;
}

    //save-load

//saving widgets in choosen record
int MainWindow::saveRecord(uint id)
{
    int pos = ui->browser->count();
    Record rec;
    if(ui->nameE->hasAcceptableInput() && isUnique(id, ui->nameE->text())) //if name is correct and unique
    {
        rec = formRecord();     //form record
        pos = DB->update(rec);   //update in database
        ui->statusbar->showMessage("Запись успешно сохранена", STATTIMEOUT);
    }
    else if(!ui->nameE->hasAcceptableInput())
        QMessageBox::warning(this,"Ошибка", "Введено некорректное имя");
    else
        QMessageBox::warning(this,"Ошибка", "Запись с таким именем уже существует");
    return pos;
}

//display choosen record in widgets
void MainWindow::showRecord(uint id)
{
    genderChangeIgnore = true;  //ignore changing of gender made by program
    Record rec;
    DB->record(id, rec);         //get record from database

    //setting date ranges to maximum span
    ui->bDateE->setDateRange(QDate(1500, 1, 1), QDate::currentDate());
    ui->dDateE->setDateRange(QDate(1500, 1, 1), QDate::currentDate());

    //loading content of record
    ui->bDateE->setDate(rec.BDate());
    ui->dDateE->setDate(rec.DDate());

    ui->nameE->setText(rec.Name());
    ui->aliveE->setChecked(rec.alive);
    ui->militaryE->setChecked(rec.military);
    ui->genderE->setCurrentIndex(rec.gender);
    ui->citezenshipE->setCurrentIndex(rec.citezenship);

    setParentLists(rec);

    if(rec.fatherId)
        ui->fatherE->setCurrentIndex(ui->fatherE->findData(QVariant(rec.fatherId)));
    else
        ui->fatherE->setCurrentIndex(0);

    if(rec.motherId)
        ui->motherE->setCurrentIndex(ui->motherE->findData(QVariant(rec.motherId)));
    else
        ui->motherE->setCurrentIndex(0);

    setDateRanges();

    ui->bDateE->setDate(rec.BDate());
    ui->dDateE->setDate(rec.DDate());

    genderChangeIgnore = false; //don't ignore gender change
}

//forms record with current content of widgets
Record MainWindow::formRecord()
{
    Record rec;

    rec.setName(ui->nameE->text());
    rec.setBDate(ui->bDateE->date());
    rec.setDDate(ui->dDateE->date());

    rec.alive = ui->aliveE->isChecked();
    rec.military = ui->militaryE->isChecked();
    rec.gender = ui->genderE->currentIndex();
    rec.citezenship = ui->citezenshipE->currentIndex();

    if(ui->fatherE->currentIndex() > 0)
        rec.fatherId = ui->fatherE->currentData().toUInt();
    else
        rec.fatherId = 0;

    if(ui->motherE->currentIndex() > 0)
        rec.motherId = ui->motherE->currentData().toUInt();
    else
        rec.motherId = 0;

    if(curID > 0)
        rec.id = uint(curID);

    return rec;
}

//checks
bool MainWindow::isUnique(uint id, QString name)
{
    if(name != "")
    {
        uint i = DB->idOf(name);  //get id of record with given name
        if(i && i != id)  //if record found and id don't match (not the same record)
            return 0;
    }
    return 1;
}


    //buttons

//disables(0) or enables(1) buttons
void MainWindow::enableButtons(bool b)
{
    ui->saveB->setEnabled(b);
    ui->removeB->setEnabled(b);
}


    //browser

//updates browser moving selecte line to correct position
void MainWindow::updateBrowser(int pos)
{
    pos++;
    QListWidgetItem *item= ui->browser->currentItem();
    if(item && item->data(Qt::UserRole).toUInt() == uint(curID))    //if saving existing record
    {
        rowChangedIgnore = true;
        item = ui->browser->takeItem(ui->browser->currentRow());    //remove line
        delete item;
    }

    Record rec;
    DB->record(uint(curID), rec);        //get record from database
    item = new QListWidgetItem(bLineFromRecord(rec));   //set text
    item->setData(Qt::UserRole, QVariant(rec.id));      //set id to data
    ui->browser->insertItem(pos, item);     //add line
    resetTabs();
    rowChangedIgnore = false;
    ui->browser->setCurrentRow(pos);
}

//fills browser with contents of database
bool MainWindow::fillBrowser()
{
    ui->browser->clear();   //clear browser
    bool full;

    if(DB->count())  //if there is records
    {
        enableButtons(1);
        insertHeader(DB->getLongestName()/10);   //tabs in header accordingly to longest name of record
        full = 1;
    }
    else
    {
        enableButtons(0);
        insertHeader(1);    //only one tab in header
        full = 0;
    }

    recLine currentLine;
    QListWidgetItem *item;

    QVector<recLine> lines = DB->records();  //get list of records
    QVectorIterator<recLine> line(lines);

    while(line.hasNext())   //insert every line in browser
    {
        currentLine = line.next();
        item = new QListWidgetItem(bLineFromStruct(currentLine));   //set text
        item->setData(Qt::UserRole, QVariant(currentLine.id));  //set id in data
        ui->browser->addItem(item);     //add to list
        ui->browser->setCurrentRow(0);
        resetTabs();
    }

    return full;
}

//forms browser line from structure
QString MainWindow::bLineFromStruct(recLine rec)
{
    int maxLength = DB->getLongestName();
    QString line = qstrFromTchar(rec.name);     //add name to line
    line.append(QString(checkTabs(line, maxLength),'\t'));


    line += qdateFromTchar(rec.bDate).toString("dd.MM.yyyy") + "\t";    //birth date

    if(rec.alive)   //if alive
    {
        if(rec.gender)
            line += "Жива\t";
        else
            line += "Жив\t";
    }
    else
        line += qdateFromTchar(rec.dDate).toString("dd.MM.yyyy");   //death date

    if(rec.fName[0] != 0 || rec.mName[0] != 0)  //add tab if there parents
        line += "\t";

    if(rec.fName[0] != 0)   //if father
    {
        line += qstrFromTchar(rec.fName);   //add father name
        if(rec.mName[0] != 0)
            line += ", ";   //add comma if there is mother
    }

    if(rec.mName[0] != 0)   //if mother
        line += qstrFromTchar(rec.mName);   //add mother name

    return line;
}

//forms browser line from record
QString MainWindow::bLineFromRecord(Record &rec)
{
    Record tmp;
    int maxLength = DB->getLongestName();
    QString line = rec.Name();  //add mane to line
    line.append(QString(checkTabs(line, maxLength),'\t'));  //tabs accordingly to max name length

    line += rec.BDate().toString("dd.MM.yyyy")+"\t";    //birth date

    if(rec.alive)   //if alive
    {
        if(rec.gender)
            line += "Жива\t";
        else
            line += "Жив\t";
    }
    else
        line+= rec.DDate().toString("dd.MM.yyyy");  //death date

    if(rec.fatherId || rec.motherId)
        line += "\t";

    if(rec.fatherId)    //if father
    {
        DB->record(rec.fatherId, tmp);
        line += tmp.Name(); //add father name
        if(rec.motherId)
            line += ", ";   //add comma if there is mother
    }

    if(rec.motherId)    //if mother
    {
        DB->record(rec.motherId, tmp);
        line += tmp.Name(); //add mother name
    }

    return line;
}

//updates browser line of given record
void MainWindow::updateLine(uint id)
{
    Record rec;
    QListWidgetItem *item = nullptr;
    DB->record(id, rec);                     //take record from database
    for(int i = 0; i < ui->browser->count(); i++)
    {
        item = ui->browser->item(i);
        if(item->data(Qt::UserRole).toUInt() == id)
            break;
    }
    if(item)
        item->setText(bLineFromRecord(rec));    //change text on browser item to updated line
}

//updates browser lines of children of given record
void MainWindow::updateChildren(uint id)
{
    QVector<uint> children = DB->getChildrenList(id);    //unintended loop here. FIX IT!!
    QVectorIterator<uint> i(children);
    while(i.hasNext())
        updateLine(i.next());
}

//how much tabs need to be added
int MainWindow::checkTabs(QString name, int max)
{
    float m = max - name.length();
    m /= TABLEN;
    return int(ceil(m));
}

//reinserts correct number of Tab's in every browser line
void MainWindow::resetTabs()
{
    QListWidgetItem *item = ui->browser->takeItem(0);
    delete item;

    QStringList l;
    QStringListIterator it(l);
    QString newtext, next;
    int max = DB->getLongestName();

    for(int i = 0; i < ui->browser->count(); i++)
    {
        l = ui->browser->item(i)->text().split('\t');   //split string on tab characters
        it = l;
        newtext = it.next();
        newtext += QString(checkTabs(newtext, max) - 1, '\t');    //insert correct number of tab characters
        //recover line
        while(it.hasNext())
        {
            next = it.next();
            if(next != QString('\t') && next != QString())
            {
                newtext += "\t";
                newtext += next;
            }
            if(next == QString("Жив") || next == QString("Жива"))
                newtext += "\t";
        }
        ui->browser->item(i)->setText(newtext); //replace text
    }

    insertHeader(max/TABLEN);
}

//inserts header to browser (t is number of tabs after "ФИО")
void MainWindow::insertHeader(int t)
{
    //forms line
    QString text = "ФИО";
    text += QString(t, '\t');
    text += "Дата рождения\tДата смерти\tРодители\t";

    QListWidgetItem *head = new QListWidgetItem(text);  //creating item
    ui->browser->insertItem(0, head);   //adding to browser
    head->setFlags(Qt::NoItemFlags);    //set unable to select
}

//selects record in browser which has given id
int MainWindow::selectById(uint id)
{
    int row = -1;
    for(int i = 1; i < ui->browser->count(); i++)
    {
        uint selected = ui->browser->item(i)->data(Qt::UserRole).toUInt();
        if(selected == id)
            row = i;
    }

    return row;
}

//handle closeEvent and ask if need to save
void MainWindow::closeEvent(QCloseEvent *event)
{
    if(DB && !DB->status())    //if base is ok
        DB->exit();   //report of exiting

    event->accept();
}


//slots
    //for dynamic check and adjustment

//sets lists of parents avialable for given record
void MainWindow::setParentLists(Record rec)
{
    //clearing lists
    while(ui->fatherE->count() > 1)  ui->fatherE->removeItem(ui->fatherE->count() - 1);
    while(ui->motherE->count() > 1)  ui->motherE->removeItem(ui->motherE->count() - 1);

    QString name;   //name of current parent

    QVector<QString> parents = DB->getParentsList(rec);  //parents list
    QVectorIterator<QString> iter(parents);

    while(iter.hasNext())   //fill fathers list
    {
        name = iter.next();
        if(name != "")    //while empty line wasn't met
        {
            uint id = DB->idOf(name);
            ui->fatherE->addItem(name, QVariant(id));
        }
        else
            break;
    }

    while(iter.hasNext())   //fill mothers list
    {
        name = iter.next();
        ui->motherE->addItem(name, QVariant(DB->idOf(name)));
    }
}

//check if peoples in other records can be parents of current
void MainWindow::checkForParents()
{
    QString fatherS = ui->fatherE->currentText();  //save before reset
    QString motherS = ui->motherE->currentText();

    Record rec = formRecord(); //form record of current widget state

    setParentLists(rec);    //fill lists

    ui->fatherE->setCurrentIndex(fatherS.isEmpty()? 0: ui->fatherE->findText(fatherS));      //refil reset indexes(if there were)
    ui->motherE->setCurrentIndex(motherS.isEmpty()? 0: ui->motherE->findText(motherS));
}

//sets min/max dates of birth/death according to all conditions
void MainWindow::setDateRanges()
{
    QString fatherS = ui->fatherE->currentText();  //save before reset
    QString motherS = ui->motherE->currentText();

    Record rec = formRecord();  //form record of current widget contents

    dateRanges rngs = DB->getDateRanges(rec);    //get date rangest for record


    //setting ranges to date edits
    ui->bDateE->setDateRange(qdateFromTchar(rngs.bDateMin), qdateFromTchar(rngs.bDateMax));
    ui->dDateE->setDateRange(qdateFromTchar(rngs.dDateMin), qdateFromTchar(rngs.dDateMax));

    ui->fatherE->setCurrentIndex(fatherS.isEmpty()? 0: ui->fatherE->findText(fatherS)); //refil reset indexes(if there were)
    ui->motherE->setCurrentIndex(motherS.isEmpty()? 0: ui->motherE->findText(motherS));
}

//refils lists of parents and sets date ranges
void MainWindow::resetLimits()
{
    checkForParents();
    setDateRanges();
}

//check if can change gender of current record
void MainWindow::genderChange()
{
    if(!genderChangeIgnore && curID != 0)  //if not ignoring changing of gender and there is records
    {
        if(DB->isParent(uint(curID)))    //if has children
        {
            QMessageBox::warning(this,"Ошибка", "Вы не можете изменить пол родителя");
            genderChangeIgnore = true;
            ui->genderE->setCurrentIndex(1-ui->genderE->currentIndex());    //change back
        }
    }
    genderChangeIgnore = false;
}


    //for buttons

//adding record to array
void MainWindow::add()
{
    Record rec = formRecord();
    bool unique = isUnique(0, ui->nameE->text());
    if(ui->nameE->hasAcceptableInput() && unique)   //check if name is unique and correct
    {
        int pos = DB->append(rec);     //add record to database
        curID = rec.id;
        updateBrowser(pos);

        //enable buttons
        enableButtons(1);
    }
    else if(!ui->nameE->hasAcceptableInput())
        QMessageBox::warning(this,"Ошибка", "Введено некорректное имя");
    else
        QMessageBox::warning(this,"Ошибка", "Запись с таким именем уже существует");
}

//saving current record
void MainWindow::saveR()
{
   int pos = saveRecord(uint(curID));
   updateBrowser(pos);
   updateChildren(uint(curID));
}

//opens record choosen in browser
void MainWindow::open(QListWidgetItem *item)
{
    if(item && !rowChangedIgnore) //if record selected
    {
        curID = item->data(Qt::UserRole).toUInt(); //get id from list item data
        showRecord(uint(curID));
    }
}

//removes record from database
void MainWindow::remove()
{
    int del = 0;
    if(DB->count())  //if there is records in database
    {
        if(!DB->isParent(curID))   //record is not parent
            del = 1;
        else    //is parent
        {
            QMessageBox::StandardButton response = QMessageBox::question(this,\
            "Удалить?", "Этот человек отмечен как родитель. Вы уверены, что хотите удалить эту запись?",\
            QMessageBox::StandardButtons(QMessageBox::Yes | QMessageBox::No));

            if(response == QMessageBox::Yes)    //if confirmed
                del = 2;
        }

        if(del) //if removed
        {
            QVector<uint> children = DB->getChildrenList(curID);   //list of children to remove references

            QListWidgetItem *item;  //browser item to change/delete

            DB->remove(curID);   //remove from database

            int index = ui->browser->currentRow();

            rowChangedIgnore = true;
            item = ui->browser->takeItem(index);   //remove line from browser
            rowChangedIgnore = false;

            delete item;

            if(index == ui->browser->count())   //counting next index
                index--;
            ui->browser->setCurrentRow(index);

            resetTabs();    //resetting tabs for every browser line

            item = ui->browser->currentItem();  //select and open next record
            open(item);

            if(del == 2)        //if deleting parent, remove all references in records of children
            {
                QVectorIterator<uint> i(children);
                while(i.hasNext())
                    updateLine(i.next());
            }
        }

        if(DB->count() == 0) //if no records left
            enableButtons(0);   //disable buttons
    }
}

    // for timed events

//syncronize contents of browser with database
void MainWindow::sync()
{
    if(checkStatus())
    {
        if(DB->isModified()) //check if there is modifications
        {
            rowChangedIgnore = true;
            uint id = 0;
            if(ui->browser->count() > 1)
                id = ui->browser->currentItem()->data(Qt::UserRole).toUInt();

            fillBrowser();  //reset browser

            int row = ui->browser->count() > 1? selectById(id): 1;
            if(row == -1)
            {
                rowChangedIgnore = false;
                ui->browser->setCurrentRow(1);
            }
            else
            {
                ui->browser->setCurrentRow(row);
                rowChangedIgnore = false;
            }
        }
    }
    else
        close();
}

    //for file menu

//new database
void MainWindow::onClear()
{
    if(DB->count())
    {
        QMessageBox::StandardButton response = QMessageBox::question(this, "Очистить?", "Вы уверены, что хотите очистить базу данных?", \
                                                                     QMessageBox::StandardButtons(QMessageBox::Yes | QMessageBox::No));
        if(response == QMessageBox::Yes)    //if confirmed
        {
            enableButtons(0);
            DB->clear();            //clear database
            ui->browser->clear();  //and browser
            insertHeader(1);       //add header
            curID = 0;
        }
    }
}


//fills database with records
void MainWindow::onFill()
{
    Record rec;
    int pos;

    if(isUnique(0, "Владимир Александрович Зеленский"))
    {
        rec = Record(0, "Владимир Александрович Зеленский", QDate(1978, 1, 25), 1, QDate::currentDate(), 0, 1);
        pos = DB->append(rec);
        curID = rec.id;
        updateBrowser(pos);
    }

    if(isUnique(0, "Иван Фёдорович Крузенштерн"))
    {
        rec = Record(0, "Иван Фёдорович Крузенштерн", QDate(1770, 11, 19), 0, QDate(1846, 8, 24));
        pos = DB->append(rec);
        curID = rec.id;
        updateBrowser(pos);
    }

    if(isUnique(0, "Отто Фон Бисмарк"))
    {
        rec = Record(0, "Отто Фон Бисмарк", QDate(1815, 4, 1), 0, QDate(1898, 7, 30), 0, 2);
        pos = DB->append(rec);
        curID = rec.id;
        updateBrowser(pos);
    }

    if(isUnique(0, "Николай Александрович Романов"))
    {
        rec = Record(0, "Николай Александрович Романов", QDate(1868, 5, 18), 0, QDate(1918, 7, 17), 0, 2);
        pos = DB->append(rec);
        curID = rec.id;
        updateBrowser(pos);
    }

    if(isUnique(0, "Мария Саломея Кюри"))
    {
        rec = Record(0, "Мария Саломея Кюри", QDate(1867, 11, 7), 0, QDate(1934, 7, 4), 1, 3);
        pos = DB->append(rec);
        curID = rec.id;
        updateBrowser(pos);
    }

    if(isUnique(0, "Дмитрий Иванович Менделеев"))
    {
        rec = Record(0, "Дмитрий Иванович Менделеев", QDate(1843, 2, 8), 0, QDate(1907, 2, 2));
        pos = DB->append(rec);
        curID = rec.id;
        updateBrowser(pos);
    }

    if(isUnique(0, "Кароль Юзеф Войтыла"))
    {
        rec = Record(0, "Кароль Юзеф Войтыла", QDate(1920, 5, 18), 0, QDate(2005, 4, 2), 0, 3);
        pos = DB->append(rec);
        curID = rec.id;
        updateBrowser(pos);
    }

    if(isUnique(0, "Сардана Владимировна Авксентьева"))
    {
        rec = Record(0, "Сардана Владимировна Авксентьева", QDate(1970, 7, 2), 1, QDate::currentDate(), 1);
        pos = DB->append(rec);
        curID = rec.id;
        updateBrowser(pos);
    }

    if(isUnique(0, "Ангела Доротея Меркель"))
    {
        rec = Record(0, "Ангела Доротея Меркель", QDate(1954, 7, 17), 1, QDate::currentDate(), 1, 2);
        pos = DB->append(rec);
        curID = rec.id;
        updateBrowser(pos);
    }

    if(isUnique(0, "Анна Андреевна Ахматова"))
    {
        rec = Record(0, "Анна Андреевна Ахматова", QDate(1889, 7, 23), 0, QDate(1966, 3, 5), 1);
        pos = DB->append(rec);
        curID = rec.id;
        updateBrowser(pos);
    }

    enableButtons(1);
}

