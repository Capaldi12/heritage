#include <iostream>
#include "windows.h"
#include "tchar.h"

int main()
{
    DWORD br;
    int type = 0;   //it's exit client
    //connecting to serer
    HANDLE hPipe = CreateFile(_T("\\\\.\\pipe\\heritage"),
                              GENERIC_READ | GENERIC_WRITE, 0,
                              nullptr, OPEN_EXISTING, 0, nullptr);
    if(hPipe != INVALID_HANDLE_VALUE) //if successful
        WriteFile(hPipe, &type, sizeof(type), &br, nullptr);

    CloseHandle(hPipe); //disconnecting

    return 0;
}
