#ifndef PIPEWORKS_H
#define PIPEWORKS_H

#include <winsock2.h>
#include <windows.h>
#include <QString>

#define pipeOut 10  //maximum tries while using pipe

//read from hPipe and check for errors
int readPipe(HANDLE hPipe, LPVOID pData, DWORD bSize);

//write to hPipe and check for errors
int writePipe(HANDLE hPipe, LPCVOID pData, DWORD bSize);

//write Qstring to hPipe
int writeQString(HANDLE hPipe, QString string);

//read Qstring from hPipe
int readQString(HANDLE hPipe, QString *string);

//send through socket
bool sendSock(SOCKET sc, const char* bff, int size);

//recieve from socket
bool recvSock(SOCKET sc, char* bff, int size);

//send QString through socket
bool sendQString(SOCKET sc, QString string);

//recieve QString from socket
bool recvQString(SOCKET sc, QString *string);



#endif // PIPEWORKS_H
