#include "record.h"

Record::Record()
{
    setName("");
    alive = 1;
    setBDate(QDate::currentDate());
    setDDate(QDate::currentDate());
    motherId = 0;
    fatherId = 0;
    citezenship = 0;
    gender = 0;
    military = 0;

    id = 0;
}

Record::Record(uint nid, TCHAR nname[51], TCHAR nbdate[11], bool nalive, TCHAR nddate[11],
               bool ngender, int ncit, uint nmid, uint nfid, bool nmil)
{
    _tcscpy_s(name, 51, nname);
    _tcscpy_s(bDate, 11, nbdate);

    if(nddate)
        _tcscpy_s(dDate, 11, nddate);
    else
        setDDate(QDate::currentDate());

    motherId = nmid;
    fatherId = nfid;
    citezenship = ncit;
    gender = ngender;
    alive = nalive;
    military = nmil;

    id = nid;
}

Record::Record(uint nid, TCHAR nname[51], QDate nbdate, bool nalive, QDate nddate,
               bool ngender, int ncit, uint nmid, uint nfid, bool nmil)
{
    _tcscpy_s(name, 51, nname);

    setBDate(nbdate);
    setDDate(nddate);

    motherId = nmid;
    fatherId = nfid;
    citezenship = ncit;
    gender = ngender;
    alive = nalive;
    military = nmil;

    id = nid;
}

Record::Record(uint nid, QString nname, QDate nbdate, bool nalive, QDate nddate,
               bool ngender, int ncit, uint nmid, uint nfid, bool nmil)
{
    setName(nname);

    setBDate(nbdate);
    setDDate(nddate);

    motherId = nmid;
    fatherId = nfid;
    citezenship = ncit;
    gender = ngender;
    alive = nalive;
    military = nmil;

    id = nid;
}

//comparison operations
int Record::recCmp(const Record & other) const
{
    if(!_tcscmp(bDate, other.bDate))        //if dates are equal
    {
        return _tcscmp(name, other.name);   //comparing names
    }
    else
        return _tcscmp(bDate, other.bDate); //comparing dates
}

bool Record::operator==(const Record & other)const {return !recCmp(other)    ;}
bool Record::operator!=(const Record & other)const {return recCmp(other)     ;}
bool Record::operator> (const Record & other)const {return recCmp(other) >  0;}
bool Record::operator>=(const Record & other)const {return recCmp(other) >= 0;}
bool Record::operator< (const Record & other)const {return recCmp(other) <  0;}
bool Record::operator<=(const Record & other)const {return recCmp(other) <= 0;}

QDataStream & operator<<(QDataStream &out, Record const &rec)
{    
    out << rec.id << rec.Name() << rec.alive << rec.BDate()
        << rec.DDate() << rec.motherId << rec.fatherId\
        << rec.citezenship << rec.gender << rec.military;

    return out;
}

QDataStream & operator>>(QDataStream &in, Record &rec)
{    
    QString b;
    QDate x, y;

    //reading in the same order as writing
    in >> rec.id >> b >> rec.alive >> x >> y >> rec.motherId\
       >> rec.fatherId >> rec.citezenship >> rec.gender >> rec.military;

    rec.setName(b);
    rec.setBDate(x);
    rec.setDDate(y);

    return in;
}

//name and date getters
QString Record::Name() const {return QString::fromWCharArray(name);}
QDate Record::BDate()  const {return QDate::fromString(QString::fromWCharArray(bDate), "yyyy.MM.dd");}
QDate Record::DDate()  const {return QDate::fromString(QString::fromWCharArray(dDate), "yyyy.MM.dd");}

//name and date setters
Record& Record::setName(QString nname) {name[nname.toWCharArray(name)] = 0; return *this;}
Record& Record::setBDate(QDate nbdate) {bDate[nbdate.toString("yyyy.MM.dd").toWCharArray(bDate)] = 0; return *this;}
Record& Record::setDDate(QDate nddate) {dDate[nddate.toString("yyyy.MM.dd").toWCharArray(dDate)] = 0; return *this;}


//tchar in/from converting
//  string
QString qstrFromTchar(TCHAR tcstr[]) {return QString::fromWCharArray(tcstr);}
int tcharFromQstr(TCHAR tcstr[], QString str) {int len = str.toWCharArray(tcstr); tcstr[len] = 0; return len;}
//  date
QDate qdateFromTchar(TCHAR tcdate[]) {return QDate::fromString(QString::fromWCharArray(tcdate), "yyyy.MM.dd");}
int tcharFromQdate(TCHAR tcdate[], QDate date) {int len = date.toString("yyyy.MM.dd").toWCharArray(tcdate); tcdate[len] = 0; return len;}
