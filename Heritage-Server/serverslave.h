#ifndef SERVERSLAVE_H
#define SERVERSLAVE_H

#include "server.h"

//abstract server slave. Not to be instanced
class ServerSlave
{
public:
    ServerSlave(Server* master, int id);
    virtual ~ServerSlave();

    virtual void process() = 0; //process next request - to be defined in heirs
    int id();
    virtual const char* ip();

    bool disconnected = 0;

protected:
    Server *server; //master server

    int slaveId;
};

//server slave working with client using named pip
class PipeSlave : public ServerSlave
{
public:
    PipeSlave(Server *master, int id = 0, HANDLE hClientPipe = INVALID_HANDLE_VALUE);

    void process() override; //process next request

private:
    HANDLE hPipe;   //pipe with client
};

//server slave working with client using TCP socket
class SocketSlave : public ServerSlave
{
public:
    SocketSlave(Server *master, sockaddr_in sain, int id = 0, SOCKET scClient = INVALID_SOCKET);

    void process() override; //process next request
    const char* ip() override;  //returns ip of connected client

private:
    SOCKET scClient;    //socket with client
    char ip_addr[20];        //ip of client
};

#endif // SERVERSLAVE_H
