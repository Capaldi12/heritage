#ifndef SERVER_H
#define SERVER_H

#include <QCoreApplication>
#include <QObject>
#include <QTimer>
#include <iostream>
#include "database.h"
#include "log.h"

class Server : public QObject
{
    Q_OBJECT

public:
    explicit Server(QObject *parent = nullptr, QCoreApplication *a = nullptr);
    ~Server();

    void exit();            //shutdown server
    bool exiting = 0;       //if set, client-serving threads disconnect their clients and exit

    void update(int updater);   //set last updater id
    unsigned short int port();  //returns port for socket

private:
    Database *db;           //main database
    QTimer *autosaver;      //timer to autosave
    HANDLE hPConnector;     //handle of client-to-pipe connecting thread
    HANDLE hSConnector;     //hancle of client-to-socket connecting thread

    int lastUpdater = -1;   //id of thread last updated database
    CRITICAL_SECTION sUpdate; //critical section to synchronize lastUpdater

    NOTIFYICONDATA *data;   //notify icon data

    QCoreApplication *app;  //to call quit and exit program

    bool visible;
    void showConsole(bool b);   //shows or hides console window

    void createTrayIcon();      //creates server icon in system tray (just displaying that server is here)

private slots:
    void autosave();            //saves db

friend class PipeSlave;         //to give slaves direct acsess to db
friend class SocketSlave;
};

//LRESULT CALLBACK WndProc(HWND window, UINT message, WPARAM wParam, LPARAM lParam);

#endif // SERVER_H
