#include "database.h"
#include <math.h>

Database::Database()
{
    freeID = 1;
    isMod = 0;
    loadConfig();

    //creating sync mutex
    hEdit = CreateMutex(nullptr, false, nullptr);
    if(hEdit != INVALID_HANDLE_VALUE)
        ok = load();
    else
        ok = 0;
}

Database::~Database()
{
    CloseHandle(hEdit);
    if(isMod) //save if database was modified upon destruction
        save();
}

//returns count of records in database
int  Database::count() const
{
    return db.count();
}

//appends database with record and returns position of record in sorted array
int  Database::append(Record &record)
{
    if(grabMutex())
    {
        uint newid = freeID++;      //assigning id
        record.id = newid;
        db.insert(newid, record);   //appending array with record
        isMod = 1;                  //database was modified
        int pos = getSortPos(newid);

        releaseMutex();
        return pos;   //returning position
    }
    return 0;
}

//removes record from database
void Database::remove(uint id)
{
    if(grabMutex())
    {
        db.remove(id);
        QMutableHashIterator<uint, Record> rec(db);

        if(isParent(id))    //remove references in other records
        {
            while(rec.hasNext())
            {
                rec.next();
                if(rec.value().motherId == id)
                {
                    rec.value().motherId = 0;
                }
                if(rec.value().fatherId == id)
                {
                    rec.value().fatherId = 0;
                }
            }
        }

        isMod = 1;  //database was modified
        releaseMutex();
    }
}

//updates content of given tecord and returns position of record in sorted array
int Database::update(const Record &record)
{
    if(grabMutex())
    {
        db[record.id] = record;         //replacing record in array
        isMod = 1;      //database was modified
        int pos = getSortPos(record.id);

        releaseMutex();
        return pos;
    }
    return 0;
}

//puts record with given id in record, if no such record, 0 returned
bool Database::record(uint id, Record &record) const
{
    if(db.contains(id))
        record = db[id];
    else
        return 0;
    return 1;
}

//saves database in given file
bool Database::save() const
{
    if(grabMutex())
    {
        //creating temporal file to avoid original file damage
        TCHAR newfilename[105];
        _tcscpy_s(newfilename, filename);
        _tcscat_s(newfilename, _T(".tmp"));

        //Open file in write mode
        HANDLE file = CreateFile(newfilename, GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);

        if(file == INVALID_HANDLE_VALUE)
        {
            releaseMutex();
            return 0;   //unsuccessful
        }

        int cnt = count();  //records' count
        unsigned long written;  //bytes written

        bool ok;
        ok = WriteFile(file, &cnt, sizeof(int), &written, nullptr);   //write count of records

        if(!ok) //if failed
        {
            CloseHandle(file);  //close file
            releaseMutex();
            return 0;
        }

        ok = ok && WriteFile(file, &freeID, sizeof(uint), &written, nullptr); //write free id

        QHashIterator<uint, Record> i(db);   //record container iterator
        Record rec; //current record

        while(i.hasNext())
        {
            i.next();
            rec = i.value();
            ok = ok && WriteFile(file, &rec, sizeof(Record), &written, nullptr);  //writing current record
        }

        CloseHandle(file);  //close file

        if(!ok) //if file wasn't written properly
        {
            DeleteFile(newfilename);
            releaseMutex();
            return 0;
        }

        ok = MoveFileEx(newfilename, filename, MOVEFILE_REPLACE_EXISTING);   //replace original file

        if(!ok) //if replacing failed
        {
            DeleteFile(newfilename);
            releaseMutex();
            return 0;
        }

        isMod = 0;  //database is not modified now

        releaseMutex();
        return 1;   //successful
    }
    return 0;
}

//load database from given file
bool Database::load()
{
    if(grabMutex())
    {
        //open file in read mode
        HANDLE file = CreateFile(filename, GENERIC_READ, 0, nullptr, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);

        if(file == INVALID_HANDLE_VALUE)
        {
            releaseMutex();
            return 0;   //unsuccessful
        }

        int cnt;  //records' count
        unsigned long read; //bytes read

        bool ok = ReadFile(file, &cnt, sizeof(int), &read, nullptr);    //read count of record

        if(!ok) //if failed
        {
            CloseHandle(file);  //close file
            releaseMutex();
            return 0;
        }

        if(read == 0)   //if 0 bytes read - file is empty
        {
            CloseHandle(file);  //close file
            releaseMutex();
            return 1;   //successful
        }

        ok = ReadFile(file, &freeID, sizeof(uint), &read, nullptr);    //read free id

        Record rec; //current record
        for (int i = 0; i < cnt; i++)
        {
            ok = ReadFile(file, &rec, sizeof(Record), &read, nullptr);  //read current record
            db.insert(rec.id, rec); //append container with record read
        }

        CloseHandle(file);  //close file
        isMod = 0;  //database is not modified now

        releaseMutex();
        return 1;   //successful
    }
    return 0;
}

//clear database
void Database::clear()
{
    if(grabMutex())
    {
        db.clear();
        freeID = 1;
        isMod = 1;

        releaseMutex();
    }
}

//returns sign of database was modified after load/save
bool Database::isModified() const
{
    return isMod;
}

//returns vector of lines to be displayed in database browser
const QVector<recLine> Database::records() const
{
    QVector<recLine> vec;
    recLine newLine;

    QHashIterator<uint, Record> rec(db);
    QMutableVectorIterator <recLine> line(vec);
    bool flag;  //flag of record is in correct place

    while(rec.hasNext())
    {
        rec.next();
        //making new line
        newLine.id = rec.key();
        newLine.alive = rec.value().alive;
        newLine.gender = rec.value().gender;

        _tcscpy_s(newLine.name, 51, rec.value().name);
        _tcscpy_s(newLine.bDate, 11, rec.value().bDate);
        _tcscpy_s(newLine.dDate, 11, rec.value().dDate);

        if(rec.value().fatherId)
            _tcscpy_s(newLine.fName, 51, db[rec.value().fatherId].name);
        else
            newLine.fName[0] = 0;
        if(rec.value().motherId)
            _tcscpy_s(newLine.mName, 51, db[rec.value().motherId].name);
        else
            newLine.mName[0] = 0;

        line.toFront();     //moving iterator to start of vector
        flag = 1;
        while(line.hasNext() && flag)   //correct position not found
        {
            line.next();
            if(db[line.value().id] >= db[newLine.id])   //if new record greater or equal to current
            {
                line.previous();    //return to previous position
                flag = 0;           //found correct place
            }
        }
        line.insert(newLine);   //adding new line to vector
    }

    return vec;
}

//returns length of longest name
int Database::getLongestName() const
{
    int len = 0;    //maximum length

    QHashIterator<uint, Record> rec(db);
    while(rec.hasNext())    //finding maximum of length
    {
        rec.next();
        if(rec.value().Name().length() > len)     //if current bigger
            len = rec.value().Name().length();    //current is maximum
    }

    return int(ceil(double(len+1)/TABLEN)*TABLEN);
}

//returns id of record with given name
uint Database::idOf(QString name) const
{
    QHashIterator<uint, Record> rec(db);
    while(rec.hasNext())
    {
        rec.next();
        if(rec.value().Name() == name)        //if names are the same
            return rec.value().id;     //return id
    }

    return 0;   //record not found
}

//checks if record with given id is a parent
bool Database::isParent(uint id) const
{
    bool check = 0;

    QHashIterator<uint, Record> rec(db);
    while(rec.hasNext())    //for every record
    {
        rec.next();
        //if stated as father or as mother in record
        if(rec.value().fatherId == id || rec.value().motherId == id)
            check = 1;
    }

    return check;
}

//returns struct of date ranges for record state
dateRanges Database::getDateRanges(const Record &state) const
{
    dateRanges rngs;
    QDate minB, minD, maxB, maxD;

    bool isFather = state.fatherId;
    bool isMother = state.motherId;
    const Record &father = db[state.fatherId];
    const Record &mother = db[state.motherId];

    maxD = QDate::currentDate();    //no way to be earlier

    if(isFather || isMother)//parent check
    {
        if(isFather && isMother)    //have both parents
        {
            minB = mother.BDate() > father.BDate()?
                    mother.BDate().addYears(15): father.BDate().addYears(15);   //can't born before both parents are 15
            if(father.alive || mother.alive)    //at least one alive
            {
                if(father.alive && mother.alive)    //both alive
                    maxB = maxD;    //not limited
                else if(father.alive)
                    maxB=mother.DDate();  //must born before mother die
                else
                    maxB=father.DDate().addMonths(9); //maximum 9 month after fathers death

            }
            else
            {
                maxB = mother.DDate()<father.DDate().addMonths(9)?
                        mother.DDate(): father.DDate().addMonths(9);//must born before mother die and
            }                                                       //maximum 9 month after fathers death
        }
        else if(isFather)   //have only father
        {
            minB = father.BDate().addYears(15);   //can't born before father is 15
            if(father.alive)
                maxB = maxD;
            else
                maxB = father.DDate().addMonths(9); //maximum 9 month after fathers death
        }
        else    //have only mother
        {
            minB = mother.BDate().addYears(15);   //can't born before mother is 15
            if(mother.alive)
                maxB = maxD;
            else
                maxB = mother.DDate(); //must born before mother die
        }
    }
    else    //no parents stated
    {
        maxB = maxD;
        minB = QDate(1752, 9, 14);
    }

    if(!state.alive)        //if dead
        maxB = maxB < state.DDate()? maxB: state.DDate();     //can't be born after die

    minD = state.BDate();   //can't die before born

    if(isParent(state.id))  //if parent
    {
        QHashIterator<uint, Record> i(db);
        while(i.hasNext())
        {
            i.next();
            const Record &pr = i.value();

            if(i.value().fatherId == state.id)            //if father
            {
                minD = minD > pr.BDate().addMonths(-9)? minD: pr.BDate().addMonths(-9); //can't die more than 9 month before child born
                maxB = maxB < pr.BDate().addYears(-15)? maxB: pr.BDate().addYears(-15); //can't born less than 15 years before child born
            }
            else if(i.value().motherId == state.id)       //if mother
            {
                minD = minD > pr.BDate()? minD: pr.BDate();     //can't die before child born
                maxB = maxB < pr.BDate().addYears(-15)? maxB: pr.BDate().addYears(-15); //can't born less than 15 years before child born
            }
        }
    }

    //filling structure

    tcharFromQdate(rngs.bDateMin, minB);    tcharFromQdate(rngs.bDateMax, maxB);
    tcharFromQdate(rngs.dDateMin, minD);    tcharFromQdate(rngs.dDateMax, maxD);

    return rngs;
}

//returns vector of avialable parents
QVector<QString> Database::getParentsList(const Record &state) const
{
    QVector<QString> parents;   //vector of parents
    QHashIterator<uint, Record> rec(db);
    QMap <QString, int> fathers, mothers;

    while(rec.hasNext())
    {
        rec.next();

        if(rec.value().id != state.id)    //if not the same record
            if(rec.value().BDate() <= state.BDate().addYears(-15))    //and old enough
            {
                if(rec.value().gender == 0)      //if male
                {   //alive or died less than 9 month before given record birth day
                    if((!rec.value().alive && rec.value().DDate() >= state.BDate().addMonths(-9)) || rec.value().alive)
                        fathers.insert(rec.value().Name(), 0);    //add to father list
                }
                else    //if female
                {   //alive or died after given record birth day
                    if((!rec.value().alive && rec.value().DDate() >= state.BDate()) || rec.value().alive)
                        mothers.insert(rec.value().Name(), 0);    //add to mother list
                }
            }
    }

    QMapIterator<QString, int> iter(fathers);
    while(iter.hasNext())
    {
        iter.next();
        parents.append(iter.key());     //add father to parents list
    }
    parents.append("");     //separate fathers from mothers
    iter = mothers;

    while(iter.hasNext())
    {
        iter.next();
        parents.append(iter.key());     //add mother to parents list
    }

    return parents;
}

//returns vector of children's ids
QVector<uint> Database::getChildrenList(uint id) const
{
    QVector<uint> children;
    QMap<uint, int> sorted;

    QHashIterator<uint, Record> rec(db);
    while(rec.hasNext())
    {
        rec.next();
        if(rec.value().motherId == id || rec.value().fatherId == id)
            sorted[rec.value().id] = 0;
    }

    QMapIterator<uint, int> i(sorted);
    while(i.hasNext())
    {
        i.next();
        children.append(i.key());
    }

    return children;
}

//returns position of record in sorted array
int Database::getSortPos(uint id) const
{
    int count = 0;      //count of records before given
    const Record &rec = db[id];     //record to find position
    QHashIterator<uint, Record> iter(db);

    while(iter.hasNext())
    {
        iter.next();
        if(rec>iter.value())    //counting records before given
            count++;
    }
    return count;   //count of records before given record is its position
}

//loading configurations from file
bool Database::loadConfig()
{
    TCHAR config[] = _T("config.txt");  //set config file name
    //open in read/write mod
    HANDLE file = CreateFile(config, GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
    if(file == INVALID_HANDLE_VALUE)
        return 0;   //unsuccessfuly

    unsigned long cnt;  //bytes read
    DWORD err = GetLastError(); //last error code to define wether file exists
    if(err == ERROR_ALREADY_EXISTS) //file exists
    {
        TCHAR buffer[110] = {};
        ReadFile(file, buffer, 110 * sizeof(TCHAR), &cnt, nullptr);   //read file
        buffer[cnt / sizeof(TCHAR)] = 0;    //append 0

        int to = _tcschr(buffer, _T('\r')) - buffer;    //find end of first line
        _tcsncpy(filename, buffer, to);                 //copy to filename
        filename[to] = 0;

        _stscanf(buffer + to + 2, _T("%d"), &port);     //setting port
    }
    else    //file not exists
    {
        WriteFile(file, filename, DWORD(_tcslen(filename) * sizeof(TCHAR)), &cnt, nullptr);    //write default file name to newly created file
        TCHAR portbuffer[8] = {};
        _stprintf(portbuffer, _T("\r\n%d"), port);
        WriteFile(file, portbuffer, 7 * sizeof(TCHAR), &cnt, nullptr);  //write port
    }

    CloseHandle(file);  //close file
    return 1;   //successfuly
}

//waiting and claiming mutex
bool Database::grabMutex() const
{
    DWORD res = WaitForSingleObject(hEdit, INFINITE);
    if(res == WAIT_OBJECT_0)
        return 1;

    return 0;
}

//releasing mutex
void Database::releaseMutex() const
{
    ReleaseMutex(hEdit);
}
