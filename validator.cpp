#include "validator.h"

NameValidator::NameValidator(QObject* parent)
    : QValidator (parent) { }

NameValidator::~NameValidator() { }

QValidator::State NameValidator::validate(QString &string, int &pos) const
{
    int i, len = string.length();
    bool flag = true;
    QChar ch;
    int spaceCount = 0;

    for(i=0; i<len; i++)
    {
        ch = string[i];

        if(ch.isSpace())
            spaceCount++;

        if(!ch.isLetter() && !ch.isSpace() && ch!='-')
            return QValidator::Invalid;
        if(spaceCount>2)
            return QValidator::Invalid;

        if(ch.isLetter() && (i == 0 || string[i-1].isSpace() || string[i-1] == '-') && !ch.isUpper())
            flag = false;

        if(ch.isLetter() && i != 0 && string[i-1].isLetter() && !ch.isLower())
            flag = false;

        if(ch == '-' && !(i!=0 && i != len-1 && string[i-1].isLetter() && string[i+1].isLetter() && string[i+1].isUpper()))
            flag = false;

        if(ch.isSpace() && !(i != 0 && i != len-1 && string[i-1].isLetter() && string[i+1].isLetter() && string[i+1].isUpper()))
            flag = false;
    }

    if(spaceCount != 2)
        flag = false;

    if(flag)
        return QValidator::Acceptable;

    /*if(string.contains(QRegExp("[^А-ЯЁа-яё\\s]"))) return QValidator::Invalid;
    QRegExp correct("([А-ЯЁ][а-яё]*[\\s]){2}([А-ЯЁ][а-яё]*){1}");   //only russian letters
    if(correct.exactMatch(string)) return QValidator::Acceptable;*/

    pos += 0;
    return QValidator::Intermediate;
}
