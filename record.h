#ifndef RECORD_H
#define RECORD_H

#include <QString>
#include <QDate>
#include <QDataStream>
#include <tchar.h>

#define TABLEN 8

enum class request
{
    exit,
    clear,
    append,
    update,
    remove,
    record,
    records,
    count,
    mod,
    longest,
    idof,
    isparent,
    get_ranges,
    get_parents,
    get_children,
    get_pos

};

struct recLine
{
    uint id;
    bool alive;
    bool gender;
    TCHAR name[51];
    TCHAR fName[51];
    TCHAR mName[51];
    TCHAR bDate[11];
    TCHAR dDate[11];

};

struct dateRanges
{
    TCHAR bDateMin[11];
    TCHAR bDateMax[11];
    TCHAR dDateMin[11];
    TCHAR dDateMax[11];
};


class Record
{
public:
    Record();
    Record(uint nid, TCHAR nname[51], TCHAR nbdate[11], bool nalive = 1, TCHAR nddate[11] = nullptr,
           bool ngender = 0, int ncit = 0, uint nmid = 0, uint nfid = 0, bool nmil = 0);

    Record(uint nid, TCHAR nname[51], QDate nbdate, bool nalive = 1, QDate nddate = QDate::currentDate(),
           bool ngender = 0, int ncit = 0, uint nmid = 0, uint nfid = 0, bool nmil = 0);

    Record(uint nid, QString nname, QDate nbdate, bool nalive = 1, QDate nddate = QDate::currentDate(),
           bool ngender = 0, int ncit = 0, uint nmid = 0, uint nfid = 0, bool nmil = 0);


    //fields
    TCHAR name[51];
    TCHAR bDate[11];    //birth date
    TCHAR dDate[11];    //death date

    uint motherId, fatherId;
    uint id;

    int citezenship;
    bool gender;
    bool alive;
    bool military;

    //comparison operations
    int recCmp(const Record & other) const;
    bool operator==(const Record & other) const;
    bool operator!=(const Record & other) const;
    bool operator> (const Record & other) const;
    bool operator>=(const Record & other) const;
    bool operator< (const Record & other) const;
    bool operator<=(const Record & other) const;

    //name and date getters
    QString Name() const;
    QDate BDate() const;
    QDate DDate() const;

    //name and date setters
    Record& setName(QString nname);
    Record& setBDate(QDate nbdate);
    Record& setDDate(QDate nddate);

    //stream input/output operations
    friend QDataStream & operator<<(QDataStream &out, Record const &rec);
    friend QDataStream & operator>>(QDataStream &in, Record &rec);
};


QString qstrFromTchar(TCHAR tcstr[]);
int tcharFromQstr(TCHAR tcstr[], QString str);
QDate qdateFromTchar(TCHAR tcdate[]);
int tcharFromQdate(TCHAR tcdate[], QDate date);

#endif // RECORD_H
