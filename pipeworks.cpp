#include "pipeworks.h"

//read from hPipe and check for errors
int readPipe(HANDLE hPipe, LPVOID pData, DWORD bSize)
{
    bool ok = 0;
    DWORD br;
    int i;
    for(i = 0; i < pipeOut; i++)    //try to read pipeOut times
    {
        ok = ReadFile(hPipe, pData, bSize, &br, nullptr);
        if(ok && br == bSize)
            return 0;   //success
        else
        {
            DWORD err = GetLastError();
            if(err == ERROR_BROKEN_PIPE || err == ERROR_PIPE_NOT_CONNECTED)
                return 1;   //connection error
            else if(err == ERROR_NO_DATA)
                return -1;  //empty pipe (for non-blocking mode)
        }
    }
    return 2;   //other error
}

//write to hPipe and check for errors
int writePipe(HANDLE hPipe, LPCVOID pData, DWORD bSize)
{
    bool ok = 0;
    DWORD bw;
    int i;
    for(i = 0; i < pipeOut; i++)    //try to write pipeOut times
    {
        ok = WriteFile(hPipe, pData, bSize, &bw, nullptr);
        if(ok && bw == bSize)
            return 0;   //success
        else
        {
            DWORD err = GetLastError();
            if(err == ERROR_BROKEN_PIPE || err == ERROR_PIPE_NOT_CONNECTED)
                return 1;   //connection error
        }
    }
    return 2;   //other error
}

//write Qstring to hPipe
int writeQString(HANDLE hPipe, QString string)
{
    int size = string.size();
    int err = writePipe(hPipe, &size, sizeof(size));    //send string size
    if(!err)
    {
        err = writePipe(hPipe, string.data(), DWORD(size) * sizeof(QChar)); //send string content
    }

    return err;
}

//read Qstring from hPipe
int readQString(HANDLE hPipe, QString *string)
{
    int size;
    int err = readPipe(hPipe, &size, sizeof(size)); //recieve string size
    if(!err)
    {
        string->resize(size);   //resize string to incoming content
        err = readPipe(hPipe, string->data(), DWORD(size) * sizeof(QChar)); //recieve content
    }

    return err;
}

//send through socket
bool sendSock(SOCKET sc, const char* bff, int size)
{
    int ok = 0;
        ok = send(sc, bff, size, 0);
        if(ok == size)
            return 1;

        return 0;
}

//recieve from socket
bool recvSock(SOCKET sc, char* bff, int size)
{
    int ok = 0;
        ok = recv(sc, bff, size, 0);
        if(ok == size)
            return 1;

        return 0;
}

//send QString through socket
bool sendQString(SOCKET sc, QString string)
{
    int size = string.size();
    bool ok = sendSock(sc, (char*)&size, sizeof(size));    //send string size
    if(ok)
    {
        ok = sendSock(sc, (char*)string.data(), size * sizeof(QChar)); //send string content
    }

    return ok;
}

//recieve QString from socket
bool recvQString(SOCKET sc, QString *string)
{
    int size;
    bool ok = recvSock(sc, (char*)&size, sizeof(size)); //recieve string size
    if(ok)
    {
        string->resize(size);   //resize string to incoming content
        ok = recvSock(sc, (char*)string->data(), size * sizeof(QChar)); //recieve content
    }

    return ok;
}
